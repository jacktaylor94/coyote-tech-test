export const mockApiCall = (value) => {
  return new Promise((resolve, reject) =>
    setTimeout(() => {
      if (!value || value === 'error') {
        reject('The value you entered caused an error');
      }

      resolve();
    }, 800)
  );
};
