import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import EditableText from './';
import {
  defaultData,
  isSubmittingData,
  successData,
  errorData,
  actionsData,
} from './EditableText.stories';

afterEach(cleanup);

const setupContainer = (customProps) => {
  const setupProps = {
    ...defaultData,
    ...actionsData,
    ...customProps,
  };

  return <EditableText {...setupProps} />;
};

describe('<EditableText />', () => {
  it('should render without error', () => {
    const component = setupContainer();
    render(component);
  });

  it('should render the loading state correctly', () => {
    const component = setupContainer(isSubmittingData);
    const { getByTestId } = render(component);

    // this will throw an error if the loading spinner can't be found
    // so no need to expect anything
    getByTestId('loading-spinner');

    // check that the input is disabled
    const input = getByTestId('input');
    expect(input.disabled).toBe(true);
  });

  it('should render the success state correctly', () => {
    const component = setupContainer(successData);
    const { getByTestId } = render(component);

    // as above - no need to expect anything to test this is there
    getByTestId('success-icon');
  });

  it('should render the error state correctly', () => {
    const component = setupContainer(errorData);
    const { getByTestId } = render(component);

    // check for the error icon and the error message
    getByTestId('error-icon');
    getByTestId('error-message');
  });

  it('should call the handleSubmit prop on blur', () => {
    // set up a mock function so we can track it
    const handleSubmit = jest.fn();

    const component = setupContainer({ handleSubmit });
    const { getByTestId } = render(component);

    // get the input...
    const input = getByTestId('input');

    // focus on and change the input value...
    input.focus();
    fireEvent.change(input, { target: { value: 'Something new' } });

    // blur the input
    input.blur();

    expect(handleSubmit).toHaveBeenCalledWith('Something new');
  });

  it('should call the handleSubmit prop when the enter key is pressed', () => {
    // set up a mock function again
    const handleSubmit = jest.fn();

    const component = setupContainer({ handleSubmit });
    const { getByTestId } = render(component);

    const input = getByTestId('input');

    input.focus();
    fireEvent.change(input, { target: { value: 'Something new' } });

    // fire an enter key up event
    fireEvent.keyUp(input, { keyCode: 13 });

    expect(handleSubmit).toHaveBeenCalledWith('Something new');

    // we also want to check that the input has been blurred
    expect(input === document.activeElement).toBe(false);
  });
});
