import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import styles from './EditableText.module.css';
import LoadingSpinner from '../LoadingSpinner';
import { ReactComponent as Tick } from '../../images/tick.svg';
import { ReactComponent as Cross } from '../../images/cross.svg';

const EditableText = ({ isSubmitting, hasError, hasSuccess, handleSubmit }) => {
  const [value, setValue] = useState('This is some inline text');
  const inputRef = useRef(null);

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      inputRef.current.blur();
    }
  };

  return (
    <div className={styles.wrapper}>
      <input
        type="text"
        value={value}
        onChange={handleChange}
        onBlur={() => handleSubmit(value)}
        onKeyUp={(e) => handleKeyUp(e)}
        className={isSubmitting ? styles.inputSubmitting : styles.input}
        disabled={isSubmitting}
        ref={inputRef}
        data-testid="input"
      />
      {hasSuccess && (
        <Tick className={styles.notificationIcon} data-testid="success-icon" />
      )}
      {isSubmitting && <LoadingSpinner />}
      {hasError && (
        <>
          <Cross className={styles.notificationIcon} data-testid="error-icon" />
          <p className={styles.errorMessage} data-testid="error-message">
            Something went terribly wrong!
          </p>
        </>
      )}
    </div>
  );
};

EditableText.propTypes = {
  isSubmitting: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  hasSuccess: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default EditableText;
