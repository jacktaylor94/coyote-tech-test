import React from 'react';
import { action } from '@storybook/addon-actions';

import EditableText from './';

export default {
  component: EditableText,
  title: 'Editable Text',
  excludeStories: /.*Data$/,
  decorators: [
    (story) => (
      <div style={{ maxWidth: '480px', margin: '0 auto' }}>{story()}</div>
    ),
  ],
};

export const defaultData = {
  isSubmitting: false,
  hasError: false,
  hasSuccess: false,
};

export const errorData = {
  ...defaultData,
  hasError: true,
};

export const successData = {
  ...defaultData,
  hasSuccess: true,
};

export const isSubmittingData = {
  ...defaultData,
  isSubmitting: true,
};

export const actionsData = {
  handleSubmit: action('handleSubmit'),
};

export const Default = () => <EditableText {...defaultData} {...actionsData} />;

export const Error = () => <EditableText {...errorData} {...actionsData} />;

export const Success = () => <EditableText {...successData} {...actionsData} />;

export const Submitting = () => (
  <EditableText {...isSubmittingData} {...actionsData} />
);
