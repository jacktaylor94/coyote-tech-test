import React from 'react';

import styles from './LoadingSpinner.module.css';

const LoadingSpinner = () => {
  return <div className={styles.loader} data-testid="loading-spinner" />;
};

export default LoadingSpinner;
