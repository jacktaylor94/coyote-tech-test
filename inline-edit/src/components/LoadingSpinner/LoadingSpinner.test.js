import React from 'react';
import { render, cleanup } from '@testing-library/react';

import LoadingSpinner from './';

afterEach(cleanup);

const setupContainer = () => {
  return <LoadingSpinner />;
};

describe('<LoadingSpinner />', () => {
  it('should render without error', () => {
    const component = setupContainer();
    render(component);
  });
});
