import React from 'react';
import { render, cleanup, fireEvent, act } from '@testing-library/react';
import wait from 'waait';

import App from './';
import * as APIService from '../../APIService';

afterEach(cleanup);

const setupContainer = () => {
  return <App />;
};

describe('<App />', () => {
  it('should render without error', () => {
    const component = setupContainer();
    render(component);
  });

  it('should not call the API if no change is detected', () => {
    const spy = jest.spyOn(APIService, 'mockApiCall');

    const component = setupContainer();
    const { getByTestId } = render(component);

    // focus and blur the input without change
    const input = getByTestId('input');
    input.focus();
    input.blur();

    // expect the API not to have been called
    expect(spy).not.toHaveBeenCalled();
  });

  it('should call the API service if a change is detected', async () => {
    // override the default API call
    APIService.mockApiCall = () => {
      return new Promise((resolve, reject) => resolve());
    };

    const spy = jest.spyOn(APIService, 'mockApiCall');

    const component = setupContainer();
    const { getByTestId } = render(component);

    const input = getByTestId('input');

    // focus and blur the input with a valid change
    input.focus();
    fireEvent.change(input, { target: { value: 'Something new' } });
    input.blur();

    expect(spy).toHaveBeenCalled();
    // check the loading spinner shows
    getByTestId('loading-spinner');

    // wait for the next tick of the event loop (for the promise to resolve)
    await act(async () => {
      await wait(1);
    });

    // check the tick is there
    getByTestId('success-icon');
  });

  it('should catch the API error if one gets thrown', async () => {
    // override the default API call with one that throws an error
    APIService.mockApiCall = () => {
      return new Promise((resolve, reject) =>
        reject('An error (this will be logged in the test console output)')
      );
    };

    const spy = jest.spyOn(APIService, 'mockApiCall');

    const component = setupContainer();
    const { getByTestId } = render(component);

    const input = getByTestId('input');

    // focus and blur the input with a change that will cause an error
    input.focus();
    fireEvent.change(input, { target: { value: 'error' } });
    input.blur();

    // wait for the next tick of the event loop (for the promise to reject)
    await act(async () => {
      await wait(1);
    });

    // check that the API has been called
    expect(spy).toHaveBeenCalled();

    // check that the error message is there
    getByTestId('error-message');
  });
});
