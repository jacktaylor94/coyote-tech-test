import React, { useState } from 'react';

import styles from './App.module.css';
import EditableText from '../EditableText';
import { mockApiCall } from '../../APIService';

const App = () => {
  const [previousValue, setPreviousValue] = useState(
    'This is some inline text'
  );

  const [editableTextState, setEditableTextState] = useState({
    hasError: false,
    hasSuccess: false,
    isSubmitting: false,
  });

  const handleSubmit = async (value) => {
    try {
      // check if the value has changed before calling the 'API'
      if (value !== previousValue) {
        setEditableTextState({
          hasError: false,
          hasSuccess: false,
          isSubmitting: true,
        });
        await mockApiCall(value);
        setEditableTextState({
          hasError: false,
          hasSuccess: true,
          isSubmitting: false,
        });

        // update the previous value
        setPreviousValue(value);

        // show the success tick for 1500ms
        setTimeout(() => {
          setEditableTextState({
            hasError: false,
            hasSuccess: false,
            isSubmitting: false,
          });
        }, 1500);
      }
    } catch (e) {
      console.log(e);
      setEditableTextState({
        isSubmitting: false,
        hasSuccess: false,
        hasError: true,
      });
    }
  };

  return (
    <div className={styles.wrapper}>
      <h1>Inline editable text</h1>
      <p>Click on the text below to edit it inline.</p>
      <p>
        <i>
          <strong>Hint: </strong>entering an empty string or inputting the word
          'error' will result in an error message being shown
        </i>
      </p>
      <EditableText handleSubmit={handleSubmit} {...editableTextState} />
    </div>
  );
};

export default App;
